const electron = require('electron');

const { app, BrowserWindow } = require('electron')
 let window;

function createWindow (){
    window = new BrowserWindow({
        width: 900,
        heigth: 800,
        webPreferences: {
            nodeIntegration: true
        }
    });

    window.loadFile('index.html');

    window.on('closed',() =>{
    window = null;
})
//окно разработчика
//window.webContents.openDevTools();
}

app.on('ready', createWindow);

app.on('window-all-closed', () =>{
    if(process.platform !== 'darwin'){
        app.quit();
    }
})