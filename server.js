const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => res.send('Сервер запущен'))
app.listen(port, () => console.log(`Example app listening on port port!`))

// данные которые будут передоваться с сервера в программу
let now = new Date();
let myArray = ["Panteleev","Alexey",now];
//экспорт массива
module.exports = myArray;